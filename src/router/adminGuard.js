import {
    store
} from '../../store'

export default (to, from, next) => {
    if (store.getters.user && store.getters.user.email == "zbadlord@gmail.com") {
        next()
    } else {
        next('/')
    }
}